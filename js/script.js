const url = 'https://api.adviceslip.com/advice'

const number = document.querySelector('.advice-number')
const advice = document.querySelector('.advice-text')
const btn = document.querySelector('.dice')

btn.addEventListener('click', () => {
  fecthAdvice()
})

const fecthAdvice = async () => {
  advice.textContent = 'Loading...'
  try {
    const response = await fetch(url, {
      headers: {
        Accept: 'application/json',
        'User-Agent': 'learning app',
      },
    })
    if (!response.ok) {
      throw new Error('An error has occured')
    }
    const data = await response.json()
    number.textContent = `Advice #${data.slip.id}`
    advice.textContent = data.slip.advice
  } catch (error) {
    advice.textContent = 'Something went wrong ...'
  }
}
fecthAdvice()
